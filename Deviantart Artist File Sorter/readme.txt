Deviantart Artist File Sorter


Deviantart is an online art repository. Images stored there have a specific
naming format, which is "#title_by_#artistName." This script allows a user 
to quickly organize downloaded pieces into folders by artist's name.


Written in Python 3.2
Copyright Jesse Anderson, 2012-2014.