import os
import glob
import shutil

# python list files

#The folder the images are stored inside.
src = "C:\\Users\\user\\Desktop\\targetFolder"
#The folder all the files will be moved to.
dst = "_sorted"


def printFiles(source):
    print ("number of files")
    fileCounter = len(glob.glob1(source, "*.*"))
    print (fileCounter)

def returnNumberOfFiles(source):
    fileCounter = len(glob.glob1(source, "*.*"))
    return fileCounter

#Generates textfile to root of script
def generateTextfile():
    file = open('log.txt','w')
    file.write("file accessed\n")
    fileCounter = returnNumberOfFiles(src)
    file.write(str(fileCounter))
    file.close()

#Moves all DA images in a directory to a folder based on their artist name.
    #ToDo: Compensate for multiple _by_ delimiters; only use the last one.
    #ToDo: omit all duplicate file numbers- those in parenthesis like "(1)"
    #ToDo: maybe move files that end with "_1" (or similar) to a folder so that they can be checked by the user
def moveFilesInDirectory():
    delimiter = "_by_"
    directoryItems = getFileList(src)
    targetDir = os.path.join(src,dst)
    try:
        os.makedirs(targetDir)
    except OSError:
        print ("Directory exists; Passing...")
        pass
    fileList = removeDirectoriesFromList(directoryItems)
    for i in fileList:
        fileLocation = os.path.join(src,i)
        splitName = []
        artistName =""
        #tries to find if the file has an artist name (ie, is 'signed')
        if(i.find(delimiter)>0):
            splitName = i.split(delimiter)
            #find ending number suffix, if any
            if(splitName[1].find("-")>0):
                split = splitName[1].split("-")
                artistName = split[0]
            #removes the filetype the artist name
            else:
                split = splitName[1].split(".")
                artistName = split[0]
        else:
            print(i + ": Delimiter not found.")
            artistName = "_unsigned"
        artistDir = os.path.join(targetDir,artistName)
        try:
            os.makedirs(artistDir)
        except OSError:
            print ("artist directory '" + artistName + "' exists.")
        shutil.move(fileLocation,artistDir)
        print (i + " moved ok")

def getFileList(directory):
    flist = os.listdir(directory)
    return flist

def removeDirectoriesFromList(inputList):
    fileList = []
    for i in inputList:
        path = os.path.join(src,i)
        if os.path.isdir(path) == True:
            print("Removing folder " + i)
        else:
            fileList.append(i)
    return fileList

mvSrc = "G:\\testbed"
mvDst = "G:\\testbed\\destination"
def fileMover ():
    fileList = getFileList(mvSrc)
    fileList = removeDirectoriesFromList(fileList)
    for i in fileList:
        mvPath = os.path.join(mvSrc,i)
        valPath = os.path.join(mvSrc,mvDst,i)
        if (os.path.isfile(valPath)):
            print("file " + i + " exists")
        else:
            shutil.move(mvPath, mvDst)

import hashlib
#def compareFiles (fileInSource, fileInTarget)

def checkHashOfString():
    i = "testfile"
    i = i.encode('utf-8')
    h = hashlib.md5()
    h.update(i)
    h = h.hexdigest()
    print(h)

def checkHashOfString():
    i = "G:\testbed\402.jpg"
    i = i.encode('utf-8')
    h = hashlib.md5()
    h.update(i)
    h = h.hexdigest()
    print(h)
    
#fileMover()
#hashchecker()
#checkHashOfString()
#checkHashOfFile()

moveFilesInDirectory()
