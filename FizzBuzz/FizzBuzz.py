def FizzBuzz(numberOfIterations):
    for i in range (1,numberOfIterations+1):
        output = ""

        if (i%3 == 0):
            output = "Fizz"
            
        if (i%5 == 0):
            output += "Buzz"
            
        if (output == ""):
            output = str(i)

        print output


def FizzBuzzEasy(numberOfIterations):
    for i in range (1,numberOfIterations+1):
        output = str(i)

        if (i%3 == 0 and i%5 == 0):
            output = "FizzBuzz"    
        elif (i%3 == 0):
            output = "Fizz"
        elif (i%5 == 0):
            output = "Buzz"

        print output    
        
FizzBuzz(50)
