Directory List to Text File


A script to find the name of all folders in a directory and write them to
a text file.


Written in Python 3.2
Copyright Jesse Anderson, 2012-2014.