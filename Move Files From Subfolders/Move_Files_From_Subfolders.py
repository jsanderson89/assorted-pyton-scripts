import os
import shutil

#Enter your folder path using two backslashes
src="C:\\Users\\User\\Desktop\\targetFolder"

fileErrors=0
listOfErrors=[]

#print list of everything in directory
for dir in os.listdir(src):
    directoryPath = (src+'\\'+ dir)

    for i in os.listdir(directoryPath):
            innerFile = (directoryPath + '\\' + i)

            try:
                shutil.move(innerFile, src)
            except shutil.Error as exception:
                print ("+++++++++++++++++++++++++++++++++++++++++++")
                print ("ERROR: could not move " + innerFile)
                listOfErrors.append(innerFile)
                fileErrors+=1
                


print(str(fileErrors) + ' errors.');

for i in listOfErrors:
    print(i)
