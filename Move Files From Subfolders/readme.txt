Move Files From Subfolder



A script used to bubble up files from within subfolders to their root
directory.


Written in Python 3.2
Copyright Jesse Anderson, 2012-2014.