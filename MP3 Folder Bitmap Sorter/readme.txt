MP3 Folder Bitmap Sorter


A python script for organizing MP3 files in a directory by bitrate. 


	-Requires the Mutagen module, which can be found here:
		https://code.google.com/p/mutagen/

	-Requires Python 2.7 (It seems Mutagen can only be installed under
	 this version.)


Written in Python 2.7
Copyright Jesse Anderson, 2014.