#version: python 2.7
#This script requires the Mutagen module, found here: https://code.google.com/p/mutagen/

import os
import shutil

try:
    import mutagen
    mutagen_loaded = True

except ImportError:
    print "Mutagen module not found. Exiting..."
    quit()
    
from mutagen.mp3 import MP3


##This is the folder the script will scan
##All albums should be sorted into folders by Artist's name
##Folder format: targetFolder\\#artistName\\#album1, #album2, #album3
#Enter your folder path using two backslashes
targetFolder = "C:\\YourFolderPath\\Folder"





def AlbumSorter(target):
#   read artist directory
    print "AlbumSorter"
    for dir in os.listdir(target):
        artistDirectory = target + '\\' + dir
        if dir != "[SORTED]":
            if os.path.isdir(artistDirectory):
##                print artistDirectory;
                stepThroughAlbums(artistDirectory)
            else:
                print "*ERROR! " + artistDirectory + " is not directory. Skipping..."



    
def stepThroughAlbums(artistDirectory):
        for albumDir in os.listdir(artistDirectory):
            print "*************************************************"
            print "*************************************************"
            albumDirectory = artistDirectory + '\\' + albumDir
            if os.path.isdir(albumDirectory):
##                  print albumDirectory;
                    stepThroughAlbumContents(albumDirectory)
            else:
                    print "**ERROR! " + albumDirectory + " is not directory. Skipping..."
                    

def stepThroughAlbumContents(albumDirectory):
    folderContents = [];
    bitrates = [];

    flac = False;
    mp3 = False;
    mp4 = False;
    
    for i in os.listdir(albumDirectory):
        path = albumDirectory + "\\" + i
##        print path

        #offers handling for subfolders - ie, two-CD albums
        if os.path.isdir(path):
            for j in os.listdir(path):
                underPath = path + "\\" + j
                folderContents.append(underPath);
        else:
            folderContents.append(path);
        

        

    for i in folderContents:
        extension = os.path.splitext(i)[1][1:]
##        print extension

        if extension == "flac":
            flac = True;

        if extension == "m4a":
            mp4 = True;
        
        if extension == "mp3":
            mp3 = True;
            bitrate = getBitrateFromFile(i)
            bitrates.append(bitrate)

    flags = [mp3, mp4, flac]
    targetPath = getTargetDirectory(flags,bitrates,albumDirectory)
    performFinalMove(albumDirectory,targetPath);

def getBitrateFromFile(mp3Path):
##    print mp3Path
    try:
        audio = MP3(mp3Path);
        bitrate = audio.info.bitrate;
        bitrate = bitrate/1000
##        print "bitrate: " + str(bitrate)
        return bitrate;
    except IOError as exception:
        print "FILE ERROR! File not found - filename likely contains special characters."
        return "";
        


def getTargetDirectory(flags, bitrates, albumDirectory):
    print "*********getTargetDirectory*********"

    directorySplit = albumDirectory.split('\\')
##    print directorySplit
##
    artistName = directorySplit[len(directorySplit)-2]
##    print "Artist Name"
##    print artistName;
##    
    albumName = directorySplit[len(directorySplit)-1]
##    print "Album Name"
##    print albumName;
##    
    artistDirectory = os.path.dirname(albumDirectory);
##    print "Arist Directory"
##    print artistDirectory;
##    
    rootDirectory = os.path.dirname(artistDirectory);
##    print "Root Directory"
##    print rootDirectory;

    folderName = "";
    
    #folder contains FLAC audio
    if (flags[0] == False and flags[1] == False and flags[2]==True):
        #move albumDirectory to artist folder in FLAC folder in root
        folderName =  "\\[FLAC]\\"
        
    elif (flags[0] == False and flags[1] == True and flags[2]==False):
        #move albumDirectory to artist folder in MP4 folder in root
        folderName = "\\[M4A]\\"

    elif (flags[0] == True and flags[1] == False and flags[2]==False):
        #move albumDirectory to designated bitrate folder in root
        finalBitrate = compareBitrates(bitrates)
##        print "Final Bitrate"
##        print finalBitrate
        
        if finalBitrate != "VBR" and finalBitrate < 192:
            folderName =  "[Less than 192]"

        else:
            folderName = "[" + str(finalBitrate) + "]"

    if folderName != "":
        targetDirectory = rootDirectory + "\\" + "[SORTED]" + "\\" + folderName + "\\" + artistName + "\\" + albumName
    else:
        targetDirectory = rootDirectory + "\\" + "\\" + artistName + "\\" + albumName
    
    print "Target Directory"
    print targetDirectory
    return targetDirectory

        
def compareBitrates(bitrates):
    albumBitrate = bitrates[0];
    
    for i in bitrates:
        if i == "":
            albumBitrate = "ERROR"
            return albumBitrate;
        elif i != bitrates[0]:
            albumBitrate = "VBR"
            return albumBitrate;

    return albumBitrate;


def performFinalMove(source,path):
    print "*********FINAL MOVE*********"
    try:
        shutil.move(source, path)

        print "MOVED: " + source;

        print os.listdir(os.path.dirname(source));
        if os.listdir(os.path.dirname(source)) == [] or os.listdir(os.path.dirname(source)) == ['Thumbs.db']:
                print "Folder is now empty. Discarding..."
                shutil.rmtree(os.path.dirname(source));
                
    except shutil.Error as exception:
        print "Error!: could not move " + source
    except WindowsError as winException:
        print "WindowsError!: could not move " + source



AlbumSorter(targetFolder)
