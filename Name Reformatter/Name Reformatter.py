src="C:\\Users\\user\\Desktop\\targetFile.txt"
out="C:\\Users\\user\\Desktop\\targetFile_formatted.txt"

fileInfo = open(src, 'r')
fileData = fileInfo.readlines()
fileInfo.close()

outputList = []

for i in fileData:
    item = []
    outStr = i.rstrip()
    outStr = outStr.split(' ')
    if len(outStr) > 2:
        item.append(outStr[2])
        item.append(' ')
    item.append(outStr[1])
    item.append(', ')
    item.append(outStr[0])
    s = ''.join(item)
    outputList.append(s)

with open(out, 'w') as outFile:
    outFile.write('\n'.join(outputList))

if(outFile.closed):
    print("file write okay")
