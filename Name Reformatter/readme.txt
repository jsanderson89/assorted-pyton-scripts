Name Reformatter


In the past, I have been required to input lists of names sent from an
outside source. Occasionally, this data is recieved in an incorrect format.
This script was created to convert a list of names in "#firstname #lastname"
format to "#lastname, #firstname" format.


Written in Python 3.2
Copyright Jesse Anderson, 2012-2014.