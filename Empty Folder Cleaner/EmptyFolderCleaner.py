import os
import shutil

#Enter your folder path using two backslashes
targetFolder = "C:\\YourFolderPath\\Folder"

def EmptyFolderFinder(target):
    for dir in os.listdir(target):
        source = target + "\\" + dir
        if (os.listdir(source) == ['Thumbs.db'] or os.listdir(source) == ['desktop.ini']  or os.listdir(source) == ['desktop.ini', 'Thumbs.db']):
            
            try:
                shutil.rmtree(os.path.dirname(source));
                
            except shutil.Error as exception:
                print "Error!: could not move " + source
                
            except WindowsError as winException:
                print "WindowsError!: could not move " + source

EmptyFolderFinder(targetFolder);
