Empty Folder Cleaner


A simple python script to clean out any empty folders in a directory. This
includes those that contain only the "Thumbs.db" Windows system file.


Written in Python 2.7
Copyright Jesse Anderson, 2014.